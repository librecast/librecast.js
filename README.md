# librecast.js

A complete rewrite of librecast.js with no dependencies and a simpler build
environment.

## License

GPL2 or, at your option GPL3
