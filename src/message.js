// eslint-disable-next-line no-unused-vars
const Message = class {
  constructor (data) {
    this.opcode = lc.OP_NOOP
    this.data = data
    if (data instanceof ArrayBuffer) {
      this.len = data.byteLength
    } else {
      this.len = (this.data === undefined) ? 0 : data.length
    }
    this.id = 0
    this.id2 = 0
    this.token = 0
  };

  get utf8 () {
    if (this.data !== undefined) {
      const decoder = new TextDecoder('utf-8')
      return decoder.decode(new Uint8Array(this.data)).slice(lc.HEADER_LENGTH)
    }
  }
}
